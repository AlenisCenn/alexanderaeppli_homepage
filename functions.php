<?php
// Nav menu
function register_my_menu()
{
  register_nav_menu('primary', __('Primary Menu'));
}
add_action('init', 'register_my_menu');

/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init()
{

  register_sidebar(array(
    'name'          => 'footer',
    'id'            => 'footer_1',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
  ));
}
add_action('widgets_init', 'arphabet_widgets_init');

// Thumbnail support
add_theme_support('post-thumbnails');
add_theme_support( 'align-wide' );

// remove auto paragraph wrapping
// remove_filter('the_content', 'wpautop');
// remove_filter('the_excerpt', 'wpautop');

// Custom post type for portfolio
function create_post_type()
{
  register_post_type(
    'cenn-portfolio',
    array(
      'labels' => array(
        'name' => __('Projekte'),
        'singular_name' => __('Projekt')
      ),
      'public' => true,
      'has_archive' => false,
      'taxonomies'  => array('category'),
      'supports' => array(
        'title',
        'editor',
        'excerpt',
        'thumbnail',
        'custom-fields',
        'revisions'
      )
    )
  );
}
add_post_type_support('cenn-portfolio', 'thumbnail');
add_action('init', 'create_post_type');

// 

function portfolio_output()
{ ?>
  <a class="portfolio-link" href="<?php the_permalink(); ?>">
    <?php the_post_thumbnail() ?>
    <h3><?php the_title() ?></h3>
  </a>
<?php
}

// [portfolio] shortcode
function portfolio_func()
{
  ob_start();

  $loop_portfolio = new WP_Query(
    array(
      'post_type' => 'cenn-portfolio',
      'posts_per_page' => -1
    )
  );
  ?>
  <!-- <div id="filter">
  <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="mbppt-ajax-filter">
    <?php
    // if ($terms = get_terms(array('taxonomy' => 'category'))) :
    //   echo '<div class="mbppt-topic-filter">';
    //   foreach ($terms as $term) :
    //     echo '<label class="mbppt-check-container tag checkbox"><input type="checkbox" name="categoryfilter[]" value="' . $term->term_id . '"><span class="mbppt-tagmark ' . $term->slug . '">' . $term->name . '</span></label>'; // ID of the category as the value of an option
    //   endforeach;
    //   echo '</div>';
    // endif;
    ?>
    <div class="mbppt-action-ajax-filter">
      <button class="button mbppt-button primary is-outline"><?php _e('Apply filter', 'myblueplanet-post-types') ?></button>
      <input type="hidden" name="action" value="action_filter">
    </div>
  </form> -->
    <div class="grid-3 justify" id="response">
      <?php while ($loop_portfolio->have_posts()) : $loop_portfolio->the_post();
        echo portfolio_output();
      endwhile;
      wp_reset_query(); ?>
    </div>
    <?php return ob_get_clean();
}
add_shortcode('portfolio', 'portfolio_func');

// Prossess ajax request
add_action('wp_ajax_myfilter', 'misha_filter_function'); // wp_ajax_{ACTION HERE} 
add_action('wp_ajax_nopriv_myfilter', 'misha_filter_function');

function misha_filter_function()
{
  // for taxonomies / categories
  $args = array(
    'post_type' => 'cenn-portfolio',
    'posts_per_page' => -1
  );
  if (isset($_POST['categoryfilter']))
    $args['tax_query'] = array(
      array(
        'taxonomy' => 'category',
        'field' => 'id',
        'terms' => $_POST['categoryfilter']
      )
    );

  $query = new WP_Query($args);

  if ($query->have_posts()) :
    while ($query->have_posts()) : $query->the_post();
      echo portfolio_output();
    endwhile;
    wp_reset_postdata();
  else :
    echo 'No posts found';
  endif;

  die();
}
