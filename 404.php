<?php get_header() ?>
<div id="content" class="container">
  <div class="nav-sub">
    <a href="https://www.alexanderaeppli.ch">
      <h1>
        <span class="black">Alexander</span>
        <span class="light">Aeppli</span>
      </h1>
    </a>
    <button class="back-btn" type="button" onmouseenter="arrowEnter()" onmouseleave="arrowLeave()" onclick="window.history.back();"><span
        id="arrow" class="fa fa-chevron-left"></span> Zurück</button>
  </div>
  <h1>
    Error 404
  </h1>
  <h2>Leider konnte die Seite nicht gefunden werden.</h2>

<?php get_footer() ?>