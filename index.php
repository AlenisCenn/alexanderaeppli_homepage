<?php get_header() ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div id="content" class="container">
  <div class="nav-sub">
    <a href="https://www.alexanderaeppli.ch">
      <h1>
        <span class="black">Alexander</span>
        <span class="light">Aeppli</span>
      </h1>
    </a>
    <button class="back-btn" type="button" onmouseenter="arrowEnter()" onmouseleave="arrowLeave()" onclick="window.history.back();"><span
        id="arrow" class="fa fa-chevron-left"></span> Zurück</button>
  </div>
  <h1>
    <?php the_title() ?>
  </h1>
  <div class="containter">
    <?php the_content() ?>
  </div>
<?php endwhile; else : ?>
<p>
  <?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?>
</p>
<?php endif; ?>

<?php get_footer() ?>