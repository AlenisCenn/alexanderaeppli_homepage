<div id="footer">
  <?php dynamic_sidebar( 'footer_1' ); ?>
  <p id="shameless_plug">Website created by Alexander Aeppli</p>
</div>
</div>
<div id="outdated">
    <h6>Ihr Browser ist veraltet!</h6>
    <p>Bitte aktualisieren Sie Ihren Browser, um diese Website korrekt darzustellen.
      <a id="btnUpdateBrowser" href="http://outdatedbrowser.com/de">Den Browser jetzt aktualisieren </a>
    </p>
    <p class="last">
      <a href="#" id="btnCloseUpdateBrowser" title="Schließen">&times;</a>
    </p>
  </div>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="<?php bloginfo('template_directory')?>/js/outdatedbrowser.min.js"></script>
  <script>
    // Outdated Browsers
    //event listener: DOM ready
    function addLoadEvent(func) {
      var oldonload = window.onload;
      if (typeof window.onload != 'function') {
        window.onload = func;
      } else {
        window.onload = function () {
          if (oldonload) {
            oldonload();
          }
          func();
        }
      }
    }
    //call plugin function after DOM ready
    addLoadEvent(function () {
      outdatedBrowser({
        bgColor: '#f25648',
        color: '#ffffff',
        lowerThan: 'transform',
        languagePath: ''
      })
    });
  </script>
  <script src="<?php bloginfo('template_directory')?>/js/jquery-3.3.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/typeit@5.10.1/dist/typeit.min.js" integrity="sha256-wV/fTluTFUM+Lv1nAc3DgjOOZlo12Dlf8Rsn2x/LU08=" crossorigin="anonymous"></script>
  <script src="<?php bloginfo('template_directory')?>/js/particles.min.js"></script>
  <script src="<?php bloginfo('template_directory')?>/js/script.js"></script>
  <?php wp_footer() ?>
</body>

</html>