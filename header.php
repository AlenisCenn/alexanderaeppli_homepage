<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Mono|Source+Sans+Pro:300,400,900" rel="stylesheet">
  <link rel="stylesheet" href="<?php bloginfo('template_directory')?>/outdatedbrowser.min.css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-134346011-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-134346011-1');
  </script>

  <title><?php bloginfo('name')?> | <?php bloginfo('description')?></title>
  <?php wp_head() ?>
  <link rel="stylesheet" href="<?php bloginfo('template_directory')?>/style.css">
</head>

<body data-spy="scroll" data-target="#menu" data-offset="0">
